//********************************************************************
// Author: James Anthony Ortiz	
//
// Program: Ch4Prob1.cpp
//
//********************************************************************


#include <iostream> //Processor Directive for input/output
#include <string>   //String library to use string types

using namespace std;

int main() //main
{
	//Data struct: for student information (first name,
	//last name, and age)
	typedef struct
	{
		string firstName;
		string lastName;
		char grade;
		int Age;
	}person;

	//Creates a member to use the stucture
	person student;

	//Prompts the user to enter his/hers
	//first name, last name, grade, and age

	cout << "What is your first name? ";
	cin >> student.firstName;

	cout << "What is your last name? ";
	cin >> student.lastName;

	cout << "What letter grade do you deserve? ";
	cin >> student.grade;

	cout << "What is your age? ";
	cin >> student.Age;

	cout << "Name: " << student.lastName << ", " << student.firstName << endl;

	//Shows one grade less than the grade that is entered as input by the user:
	if (student.grade == 'A')
	{
		cout << "Grade: B" << endl;
	}
	else if (student.grade == 'B')
	{
		cout << "Grade: C" << endl;
	}
	else if (student.grade == 'C')
	{
		cout << "Grade: D" << endl;
	}
	else if (student.grade == 'D')
	{
		cout << "Grade: F" << endl;
	}

	//Displays age:
	cout << "Age: " << student.Age << endl;

	cin.get();
	cin.get();
	return 0; 

} //End main